﻿import discord
from discord.ext import commands
import datetime
import sqlite3

class FutureGadgetLab:
    """This is the Development module of Amadeus-Kurisu. This module contains all of the general purpose development commands for Amadeus-Kurisu."""

    def __init__(self, client):
        self.client = client
        print("Addon \"{}\" loaded.".format(self.__class__.__name__))
        client.cursor.execute('''CREATE TABLE IF NOT EXISTS fglchallenges (

            ChallengeName varchar(255),
            StartTime int,
            EndTime int,
            Status varchar(255),
            Trigger varchar(255)



            )
        ''')

        client.database.commit()

        """self.client.cursor.execute("SELECT * FROM fglchallenges WHERE ChallengeName=?", ("Stay Single"))
        query = self.client.cursor.fetchone()
        if query is None:
            self.client.cursor.execute("INSERT INTO fglchallenges VALUES (?,?,?,?)", ())
            self.client.database.commit()
        """
        
        
    @commands.command(pass_context=True, name="matchachallenge", aliases=["matchasingle", "matchanoyuri"])
    async def singlechallenge(self, context):
        """Prints the status on whatever mess Matcha has gotten herself into this time.
        
        Command exclusive to the FGL Server, and can be enabled or disabled."""

        fgl_server_id = "84792070480330752"

        self.client.cursor.execute("SELECT * FROM config WHERE Setting='perms' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        #print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            print(context.message.server.id)
            if context.message.server.id == fgl_server_id:
                print("passed")

                ## Start Date Timestamp 1506729600
                ## Finish Date Timestamp 1592870400


                start_date = datetime.date(2017,9,30)
                end_date = datetime.date(2020,6,23)

                days_since_start = datetime.now() - start_date 
                days_to_go = end_date - days_since_start
                


                challenge_embed = discord.Embed(color=discord.Colour.dark_teal())


            
                challenge_embed.set_author(name="Matcha's \"Stay Single\" Challenge ", icon_url=context.message.server.icon_url)
                challenge_embed.add_field(name="Started:", value="September 30th, 2017", inline=False)
                challenge_embed.add_field(name="Days since start:", value=days_since_start, inline=False)
                challenge_embed.add_field(name="Days to go: ", value=days_to_go, inline=False)
                challenge_embed.add_field(name="Finishes:", value="June 23rd, 2020", inline=False)
                challenge_embed.add_field(name="Status:", value="Ongoing", inline=False)
                
                
                
                

                await self.client.say(embed=challenge_embed)
            else:
                print("not passed")
            
        

def setup(client):
    client.add_cog(FutureGadgetLab(client))