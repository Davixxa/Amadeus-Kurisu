from discord.ext import commands
import asyncio
import sqlite3
import discord
import time
import modules.responses
import modules.utils
import math 


## Dummy Test Comment

class Admin:
    """This is the Admin module of Amadeus-Kurisu. This module contains all of the nessecary commands for administering Amadeus-Kurisu on your server."""

    def __init__(self, client):
        self.client = client
        self.utils = modules.utils
        self.responses = modules.responses
        self.valid_commands = ["ping", "ohmy", "lenny", "meme", "checkperms", "robots", "pinquote", "8ball", "log", "serverinfo", "ripme", "nosememes", "nullpo", "markov", "maholog"]
        self.timers_storage = client.unmute_timers
        print("Addon \"{}\" loaded.".format(self.__class__.__name__))

        self.client.cursor.execute("SELECT * FROM config WHERE Setting='SilencedRoleID'") # Fetch Bot Commander Role ID.
        query_results = self.client.cursor.fetchall()
        print(query_results)
        mute_role_ids = []
        for result in query_results:
            mute_role_ids.append(result[3])
        
            
        print("Silenced Roles: " + str(mute_role_ids))
        cursor = self.client.cursor

        #Thanks Emoji once again for having your bot be open source, so I can shamelessly borrow most of the code for this
        #Check for members to be unmuted now.
        self.client.cursor.execute("SELECT * FROM mutes WHERE MuteTime < strftime('%s','now')")
        users_to_unmute_now_query = cursor.fetchall()
        print("Users to unmute: " + str(users_to_unmute_now_query))
        if users_to_unmute_now_query:
            unmute_tasks = []
            print("Users with expired mutes found. Removing...")
            for row in users_to_unmute_now_query:
                print(row[3])
                for server in self.client.servers:
                    #print("Server ID: " + server.id)
                    if str(server.id) == str(row[3]):
                        #print("Server reached")
                        #print("Server ID: " + row[3])
                        self.client.cursor.execute("SELECT * FROM config WHERE Setting='SilencedRoleID' AND ServerID=?", (server.id,))
                        role_id = self.client.cursor.fetchone()[3]
                        #print(role_id)
                        silenced_role = discord.utils.find(lambda r: r.id == role_id, server.roles)
                        #print(silenced_role.name)
                        member = server.get_member(str(row[0]))
                        task = asyncio.ensure_future(self.client.remove_roles(member, silenced_role))
                        unmute_tasks.append(task)
                        task.add_done_callback(unmute_tasks.remove)
                        self.client.loop.create_task(self.init_role_removal(member, server))
                        break
            self.client.cursor.execute("DELETE FROM mutes WHERE MuteTime < strftime('%s','now')")
            self.client.database.commit()
            
        
            #print("Removed")
        self.client.cursor.execute("SELECT * FROM mutes")
        to_unmute_later_query = cursor.fetchall()
        if to_unmute_later_query:
            print("Users with running mute timers found")
            for row in to_unmute_later_query:
                for server in client.servers:
                    if str(server.id) == str(row[3]):
                        #print("Server reached")
                        self.client.cursor.execute("SELECT * FROM config WHERE Setting='SilencedRoleID' AND ServerID=?", (server.id,))
                        role_id = self.client.cursor.fetchone()[3]
                        #print(role_id)
                        silenced_role = discord.utils.find(lambda r: r.id == role_id, server.roles)
                        #print(silenced_role.name)                        
                        member = server.get_member(str(row[0]))
                        seconds_to_unmute = row[2] - time.time()
                        if member.id not in self.timers_storage[server.id]:
                            print("Setting up timers...")
                            unmute_timer = self.client.loop.create_task(self.unmute_timer(None, server, member, silenced_role, seconds_to_unmute))
                            self.timers_storage[server.id].update({member.id: unmute_timer})


    
    @commands.command(pass_context=True)
    async def enable(self, context, command=None):
                """You can enable another command with this command. This command requires you to either be the bot owner, be a bot commander or have the \"Manage Server\" Permission. """
                self.client.cursor.execute("SELECT * FROM config WHERE Setting='BotCommanderRoleID' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
                commander_id = self.client.cursor.fetchone()[3]
                commander_role = discord.utils.find(lambda r: r.id == commander_id, context.message.author.roles)
                print(commander_role)
                if commander_role != None or context.message.author.server_permissions.manage_server or context.message.author.server_permissions.administrator:
                    args_used = len(context.message.content.split("enable ")[1].split(" "))
                    print(args_used)
                    if args_used is not 1:
                        await self.client.say(self.utils.randomize_string(self.responses.invalid_argument_count, {"%name%": context.message.author.mention, "%arguments_required%": "1", "%arguments_used%": str(args_used)}))
                    else:
                        if command not in self.valid_commands:
                            valid_command_string = """"""
                            for valid_command in self.valid_commands:
                                valid_command_string += valid_command + " \n"
                            await self.client.say("""Invalid Command. Valid Commands are: ```""" + valid_command_string + "```")
                        else:
                            self.client.cursor.execute("SELECT * FROM config WHERE Setting=? AND ServerID=?", (command,context.message.channel.server.id,)) # Fetch current enabled/disabled state from server.
                            query = self.client.cursor.fetchone()
                            print(query)
                            #print(value)

                            if query != None: #Once again, check if there's ever been one there.
                                self.client.cursor.execute("UPDATE config SET Value=\"Enabled\" WHERE ServerID=? AND Setting=?", (context.message.channel.server.id, command)) #Else set the Bot Commander Role ID to value.
                                self.client.database.commit()
                                await self.client.say("Command " + command + " enabled.")
                                    
                            else: 
                                config_change = (context.message.channel.server.id, context.message.channel.server.name, command, "Enabled") #If the query does in fact, not exist, just insert it instead of update.
                                self.client.cursor.execute("INSERT INTO config VALUES (?,?,?,?)", config_change)
                                print(config_change)
                                self.client.database.commit()
                                await self.client.say("Command " + command + " enabled.")
                            
                else:
                    await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))

                    
    @commands.command(pass_context=True)
    async def disable(self, context, command=None):
            """You can disable another command with this command. This command requires you to either be the bot owner, be a bot commander or have the \"Manage Server\" Permission. """
            self.client.cursor.execute("SELECT * FROM config WHERE Setting='BotCommanderRoleID' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
            commander_id_obj = self.client.cursor.fetchone()
            if commander_id_obj is None and not context.message.author.server_permissions.manage_server and not context.message.author.server_permissions.administrator:
                await self.client.say("Bot Commander is not configured on this server")
            else:
                pass
            commander_id = commander_id_obj[3]
            commander_role = discord.utils.find(lambda r: r.id == commander_id, context.message.author.roles)
            print(commander_role)
            if commander_role != None or context.message.author.server_permissions.manage_server or context.message.author.server_permissions.administrator:
                args_used = len(context.message.content.split("disable ")[1].split(" "))
                print(args_used)
                if args_used is not 1:
                    await self.client.say(self.utils.randomize_string(self.responses.invalid_argument_count, {"%name%": context.message.author.mention, "%arguments_required%": "1", "%arguments_used%": str(args_used)}))
                else:
                    if command not in self.valid_commands:
                        valid_command_string = """"""
                        for valid_command in self.valid_commands:
                            valid_command_string += valid_command + " \n"
                        await self.client.say("""Invalid Command. Valid Commands are: ```""" + valid_command_string + "```")
                    else:                    
                        self.client.cursor.execute("SELECT * FROM config WHERE Setting=? AND ServerID=?", (command,context.message.channel.server.id,)) # Fetch current enabled/disabled state from server.
                        query = self.client.cursor.fetchone()
                        print(query)
                        #print(value)

                        if query != None: #Once again, check if there's ever been one there.
                            self.client.cursor.execute("UPDATE config SET Value=\"Disabled\" WHERE ServerID=? AND Setting=?", (context.message.channel.server.id, command)) #Else set the Bot Commander Role ID to value.
                            self.client.database.commit()
                            await self.client.say("Command " + command + " disabled.")
                                
                        else: 
                            config_change = (context.message.channel.server.id, context.message.channel.server.name, command, "Disabled") #If the query does in fact, not exist, just insert it instead of update.
                            self.client.cursor.execute("INSERT INTO config VALUES (?,?,?,?)", config_change)
                            print(config_change)
                            self.client.database.commit()
                            await self.client.say("Command " + command + " disabled.")
                    
            else:
                await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))

            
        
    @commands.command(pass_context=True)
    async def mute(self, context, user=None, time=None):
            """
            \"I told you, there's no -tina!\"

            Mutes an user for a certain length of time, measured in seconds.

            Mutes put in as forever aren't added to timers storage, therefore, the unmute error will come with a warning upon unmuting. It should still remove the mute.
            
            """
            
            self.client.cursor.execute("SELECT * FROM config WHERE Setting='BotCommanderRoleID' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
            commander_id_query = self.client.cursor.fetchone()
            commander_id = None
            if commander_id_query is not None:
                commander_id = commander_id_query[3]
            print(commander_id)
            commander_role = discord.utils.find(lambda r: r.id == commander_id, context.message.author.roles)
            print(commander_role)
            if commander_role != None or context.message.author.server_permissions.manage_server or context.message.author.server_permissions.administrator:
                self.client.cursor.execute("SELECT * FROM config WHERE Setting='SilencedRoleID' AND ServerID=?", (context.message.channel.server.id,))
                silenced_role_id_query = self.client.cursor.fetchone()
                silenced_role_id = None
                print("Passed Perm check")
                if user is not None and time is not None:
                    if silenced_role_id_query is not None:
                        silenced_role_id= silenced_role_id_query[3]
                        print(silenced_role_id, " Passed query check")
                        silenced_role = discord.utils.find(lambda r: r.id == silenced_role_id, context.message.server.roles)
                        if silenced_role is not None:
                            print("Silenced Role isn't none")
                            member_query = await self.utils.get_members(self.client, context.message, user)
                            if member_query is None:
                                return
                            
                            print("Member_Query isn't none")
                            member_object = context.message.server.get_member_named(member_query[0])
                            
                            if member_object.id in self.timers_storage[str(context.message.server.id)]:
                                await self.client.say("You dummy! This member is already muted.")
                                return
                            
                            
                            print(self.utils.is_number(time))

                            forever = False
                            
                            if time == "∞" or time == "permanent" or time =="forever" or time =="68y": 
                                time = 2147483647
                                forever = True


                            if not self.utils.is_number(time):
                                await self.client.say("Yeah. Please provide a time. Fyi, ∞, permanent, forever and 68y mutes a person for 68 years.")
                                

                            if int(time) > 2147483647:
                                await self.client.say("Nice try. Don't break the 32-bit integer limit pls kthx.")  
                            print("User isn't already muted")
                            await self.client.add_roles(member_object, silenced_role)
                            if not forever:
                                unmute_timer = self.client.loop.create_task(self.unmute_timer(context, context.message.server, member_object, silenced_role, time))
                                self.timers_storage[context.message.server.id].update({member_object.id: unmute_timer})
                                print(self.timers_storage)
                                print(unmute_timer)
                            
                                values = (member_object.id, member_object.name, time, context.message.server.id)
                                self.client.database.execute("INSERT INTO mutes(UserID, UserName, MuteTime, ServerID) VALUES (?, ?, strftime('%s', 'now') + ?, ?)", values)
                                self.client.database.commit()

                            await self.client.say(self.utils.randomize_string(self.responses.command_succeded, {"%name%": context.message.author.mention}))

                        
                    else:
                        print("Did not pass query check")
                        await self.client.say("Ugh, you idiot. You forgot to configure the command. Lurk moar... or something... (Mutedrole not set. Use `!set mutedrole <id>` to fix.)")
                else:
                    args_used = 0
                    if user is None:
                        args_used = 0
                    else:
                        if time is not None:
                            args_used = 1 
                    await self.client.say(self.utils.randomize_string(self.responses.invalid_argument_count, {"%name%": context.message.author.mention, "%arguments_required%": "2", "%arguments_used%": str(args_used +1 )}))
            else:
                await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))

    @commands.command(pass_context=True, name="set")
    async def set_setting(self, context, setting, value=None):
        """
        This command is used for setting stuff. 
        
        Currently the valid options are:

        pingmessage, which sets the message on the ping command. Takes a string.

        botcommanderrole, which sets the role for bot commanders. Takes the role ID.

        mutedrole, which sets the silenced role. Takes the role ID.
        """
        
        # Set Pin Message

        if setting.lower() == "pingmessage":
            
            self.client.cursor.execute("SELECT * FROM config WHERE Setting='BotCommanderRoleID' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
            commander_id_query = self.client.cursor.fetchone()
            commander_id = None
            if commander_id_query is not None:
                commander_id = commander_id_query[3]
            print(commander_id)
            commander_role = discord.utils.find(lambda r: r.id == commander_id, context.message.author.roles)
            print(commander_role)
            if commander_role != None or context.message.author.server_permissions.manage_server or context.message.author.server_permissions.administrator:
            
                self.client.cursor.execute("SELECT * FROM config WHERE Setting='PingMessage' AND ServerID=?", (context.message.channel.server.id,)) # Fetch current pingmessage from server

                query = self.client.cursor.fetchone()


                print(query) # Print the results from the query.
                print(value)
                if query != None: # Check if it actually exists
                    if value == None or value == "" or value == "clear" or value == "default" or value == "reset": # If value is empty, or is "clear", etc, reset the pingmessage.
                        self.client.cursor.execute("UPDATE config SET Value=NULL WHERE ServerID=? AND Setting='PingMessage'", (context.message.channel.server.id,))
                        self.client.database.commit()
                    else: 
                        self.client.cursor.execute("UPDATE config SET Value=? WHERE ServerID=? AND Setting='PingMessage'", (value, context.message.channel.server.id,)) #Else set the PingMessage to value.
                        self.client.database.commit()
                else:
                    config_change = (context.message.channel.server.id, context.message.channel.server.name, "PingMessage", value) #If the query does in fact, not exist, just insert it instead of update.
                    self.client.cursor.execute("INSERT INTO config VALUES (?,?,?,?)", config_change)
                    print(config_change)
                    self.client.database.commit()
            else:
                await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))
        # Set Bot Commander Role

        elif setting.lower() == "botcommanderrole":
            print(context.message.author.server_permissions.manage_server)
            #Check if the sender has "Manage Server" as a permission.
            if context.message.author.server_permissions.manage_server == True or context.message.author.server_permissions.administrator:
            
                self.client.cursor.execute("SELECT * FROM config WHERE Setting='BotCommanderRoleID' AND ServerID=?", (context.message.channel.server.id,)) # Fetch current bot commander role from server

                query = self.client.cursor.fetchone()

                print(query) # Print the results from the query.
                print(value)
                if query != None: # Check if it actually exists
                    if value == None or value == "" or value == "clear" or value == "default" or value == "reset": # If value is empty, or is "clear", etc, clear the bot commander role.
                        self.client.cursor.execute("DELETE FROM config WHERE ServerID=? AND Setting='BotCommanderRoleID'", (context.message.channel.server.id,))
                        self.client.database.commit()
                        await self.client.say("Bot Commander Role Cleared")
                    else: 
                        commander_role = discord.utils.find(lambda r: r.id == value, context.message.channel.server.roles)
                        if commander_role != None:
                            self.client.cursor.execute("UPDATE config SET Value=? WHERE ServerID=? AND Setting='BotCommanderRoleID'", (value, context.message.channel.server.id,)) #Else set the Bot Commander Role ID to value.
                            self.client.database.commit()
                            await self.client.say("Bot Commander Role Set to " + commander_role.name)
                        else:
                            await self.client.say("Role does not exist.")
                else:
                    config_change = (context.message.channel.server.id, context.message.channel.server.name, "BotCommanderRoleID", value) #If the query does in fact, not exist, just insert it instead of update.
                    commander_role = discord.utils.find(lambda r: r.id == value, context.message.channel.server.roles)
                    if commander_role != None:
                        self.client.cursor.execute("INSERT INTO config VALUES (?,?,?,?)", config_change)
                        print(config_change)
                        self.client.database.commit()
                        await self.client.say("Bot Commander Role Set to " + commander_role.name)
                    else:
                        await self.client.say("Role does not exist.")
                        

            else: #If the sender doesn't have said permission, refuse.
                await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))

        ############### TBI: Muted Role ########################

        elif setting.lower() == "mutedrole":
            self.client.cursor.execute("SELECT * FROM config WHERE Setting='BotCommanderRoleID' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
            commander_id = self.client.cursor.fetchone()[3]
            #print(commander_id)
            commander_role = discord.utils.find(lambda r: r.id == commander_id, context.message.author.roles)
            #print(commander_role)
            if commander_role != None or context.message.author.server_permissions.manage_server or context.message.author.server_permissions.administrator:
                #print("Permission/Role Check")
                #await self.client.say("Role Check Succeeded")
                self.client.cursor.execute("SELECT * FROM config WHERE Setting='SilencedRoleID' AND ServerID=?", (context.message.channel.server.id,)) # Fetch current silenced role id from server
                query = self.client.cursor.fetchone()
                #print(query)
                #print(value)

                if query != None: #Once again, check if there's ever been one there.
                    if value == None or value == "" or value == "clear" or value == "default" or value == "reset": # If value is empty, or is "clear", etc, clear the bot commander role.
                        self.client.cursor.execute("DELETE FROM config WHERE ServerID=? AND Setting='SilencedRoleID'", (context.message.channel.server.id,))
                        self.client.database.commit()
                        await self.client.say("Muted Role Cleared.")
                    else: 
                        muted_role = discord.utils.find(lambda r: r.id == value, context.message.channel.server.roles)
                        if muted_role != None:
                            self.client.cursor.execute("UPDATE config SET Value=? WHERE ServerID=? AND Setting='SilencedRoleID'", (value, context.message.channel.server.id,)) #Else set the Bot Commander Role ID to value.
                            self.client.database.commit()
                            await self.client.say("Muted Role Set to " + muted_role.name)
                        else:
                            await self.client.say("Role does not exist.")
                else: 
                    config_change = (context.message.channel.server.id, context.message.channel.server.name, "SilencedRoleID", value) #If the query does in fact, not exist, just insert it instead of update.
                    commander_role = discord.utils.find(lambda r: r.id == value, context.message.channel.server.roles)
                    if commander_role != None:
                        self.client.cursor.execute("INSERT INTO config VALUES (?,?,?,?)", config_change)
                        print(config_change)
                        self.client.database.commit()
                        await self.client.say("Muted Role Set to " + commander_role.name)
                    else:
                        await self.client.say("Role does not exist.")                    

            else:
                await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))
            
        elif setting.lower() == "newschannel":
            self.client.cursor.execute("SELECT * FROM config WHERE Setting='BotCommanderRoleID' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
            commander_id = self.client.cursor.fetchone()[3]
            #print(commander_id)
            commander_role = discord.utils.find(lambda r: r.id == commander_id, context.message.author.roles)
            #print(commander_role)
            if commander_role != None or context.message.author.server_permissions.manage_server or context.message.author.server_permissions.administrator:
                #print("Permission/Role Check")
                #await self.client.say("Role Check Succeeded")
                self.client.cursor.execute("SELECT * FROM config WHERE Setting='NewsChannel' AND ServerID=?", (context.message.channel.server.id,)) # Fetch current news channel id from server
                query = self.client.cursor.fetchone()
                #print(query)
                #print(value)

                if query != None: #Once again, check if there's ever been one there.
                    if value == None or value == "" or value == "clear" or value == "default" or value == "reset": # If value is empty, or is "clear", etc, clear the bot commander role.
                        self.client.cursor.execute("DELETE FROM config WHERE ServerID=? AND Setting='NewsChannel'", (context.message.channel.server.id,))
                        self.client.database.commit()
                        await self.client.say("News Channel Cleared.")
                    else: 
                        news_channel = discord.utils.find(lambda r: r.id == value, context.message.channel.server.channels)
                        if news_channel != None:
                            self.client.cursor.execute("UPDATE config SET Value=? WHERE ServerID=? AND Setting='NewsChannel'", (value, context.message.channel.server.id,)) #Else set the Bot Commander Role ID to value.
                            self.client.database.commit()
                            await self.client.say("News Announcement Channel Set to " + news_channel.name)
                        else:
                            await self.client.say("Channel does not exist.")
                else: 
                    config_change = (context.message.channel.server.id, context.message.channel.server.name, "NewsChannel", value) #If the query does in fact, not exist, just insert it instead of update.
                    news_channel = discord.utils.find(lambda r: r.id == value, context.message.channel.server.channels)
                    if news_channel != None:
                        self.client.cursor.execute("INSERT INTO config VALUES (?,?,?,?)", config_change)
                        print(config_change)
                        self.client.database.commit()
                        await self.client.say("News Announcement Channel Set to " + news_channel.name)
                    else:
                        await self.client.say("Channel does not exist.")                    

            else:
                await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))
                

    @commands.command(pass_context=True)
    async def unmute(self, context, user):
        """
        "Well at least you're not calling me a zombie anymore..."

        What this command does should be completely obvious, but if you really don't get it:

        It unmutes a user. As simple as that.


        Since mutes created as forever aren't added to the timers storage and database, the bot will behave differently, but will still unmute.

        """

        server = context.message.server

        self.client.cursor.execute("SELECT * FROM config WHERE Setting='BotCommanderRoleID' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        commander_id = self.client.cursor.fetchone()[3]
        #print(commander_id)
        commander_role = discord.utils.find(lambda r: r.id == commander_id, context.message.author.roles)
        #print(commander_role)
        if commander_role != None or context.message.author.server_permissions.manage_server or context.message.author.server_permissions.administrator:
            #print("Permission/Role Check")
            #await self.client.say("Role Check Succeeded")

            # Check permissions first.

                self.client.cursor.execute("SELECT * FROM config WHERE Setting='SilencedRoleID' AND ServerID=?", (context.message.channel.server.id,))
                silenced_role_id_query = self.client.cursor.fetchone()
                silenced_role_id = None
                print("Passed Perm check")
                if user is not None and time is not None:
                    if silenced_role_id_query is not None:
                        silenced_role_id= silenced_role_id_query[3]
                        print(silenced_role_id, " Passed query check")
                        silenced_role = discord.utils.find(lambda r: r.id == silenced_role_id, context.message.server.roles)
                        if silenced_role is not None:
                            print("Silenced Role isn't none")
                            member_query = await self.utils.get_members(self.client, context.message, user)
                            if member_query is None:
                                return
                            
                            print("Member_Query isn't none")
                            member_object = context.message.server.get_member_named(member_query[0])
                            
                            if not commands.bot_has_permissions(manage_roles=True):
                                await self.client.say("Idiot. You forgot to give me the `Manage Roles`. For the record, to set up that permission, go to Server Settings -> Roles -> Amadeus-Kurisu and check the `Manage Roles` permission. What a fucking newfag.")
                                return

                            member = server.get_member_named(member_query[0])

                            await self.client.remove_roles(member, silenced_role)
                            print("Removed Role")

                            self.remove_muted_member(context, member, server)

                        else:
                            await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))


    #@commands.command()
    #async def 

    async def unmute_timer(self, context, server, member, role, seconds: int):
        print("Reached Unmute Timer")
        print(server.id, member.id, role.id, seconds)

        try:
            print("Sleeping")
            await asyncio.sleep(int(seconds))
            print("Slept")

            await self.client.remove_roles(member, role)
            print("Removed Role")

            self.remove_muted_member(context, member, server)

            print("Member {} has been unmuted.".format(member.display_name))

        except asyncio.CancelledError as e:
            self.remove_muted_member(context, member, server)
            print(e)
            print("AsyncError excepted")
            
    def remove_muted_member(self, context, member, server):
        values = (member.id, server.id)
        self.client.cursor.execute("DELETE FROM mutes WHERE UserID=? AND ServerID=?", values)
        self.client.database.commit()
        try:
            del self.timers_storage[server.id][member.id]
            if context is not None:
                self.client.loop.create_task(self.client.send_message(context.message.channel, self.utils.randomize_string(self.responses.unmute_messages, {"'user'": member.mention}) + " (Action: Unmuted " + member.display_name + ")"))
            else:
                self.client.loop.create_task(self.init_role_removal(member, server))

        except Exception as e:
            print(e)
            print("This is most likely due to it being an infinite mute")
            
            self.client.loop.create_task(self.client.send_message(context.message.channel, "Member {} isn't in timers storage. Either the member wasn't muted in the first place, or it was an infinite mute, in which case, has been removed.".format(member.mention)))
        print("Removed Mute")



    async def init_role_removal(self, member, server):
        try:
            await self.client.send_message(server.default_channel, self.utils.randomize_string(self.responses.unmute_messages, {"'user'": member.mention}) + " (Action: Unmuted " + member.display_name + ")")
        except Exception as e:
            print(e)
            await self.client.send_message(member, self.utils.randomize_string(self.responses.unmute_messages, {"'user'": member.mention}) + " (Action: You have been unmuted)")



def setup(client):
    client.add_cog(Admin(client))