# Import Base Modules
import sqlite3
import discord
import asyncio
import config
from discord.ext import commands
command_prefix = "¤"

client = discord.Client()

connection = sqlite3.connect('kurisu.db')

c = connection.cursor()

c.execute('''CREATE TABLE IF NOT EXISTS config (
    ServerID int, 
    ServerName varchar(255),
    Setting varchar(255),
    Value varchar(255)
    )
    ''')
connection.commit()

#connection.close()

# Will be using a good chunk of Emoji's codebase for this. Way better than what I can come up with on my own.



@client.event
async def on_ready():
    print("Logged in as")
    print(client.user.name)
    print(client.user.id)
    print("-------")
    #for server in client.servers:
        #server_id = (server.id)

    # CREATE TABLE IF NOT EXISTS logs-serverid

@client.event
async def on_message(message):

    if message.author.bot:
        return #Thanks Emoji. Haven't actually thought about that.

    if message.content.lower().startswith(command_prefix + "ping"):
        c.execute("SELECT * FROM config WHERE Setting='PingMessage' AND ServerID=?", (message.channel.server.id,))
        #c.execute("SELECT * FROM config WHERE Setting='PingMessage' AND ServerID='123123123123'")
        custom_message = c.fetchone()
        print(custom_message)
        if custom_message[3] != None:
            await client.send_message(message.channel, custom_message[3])
        else:
            await client.send_message(message.channel, "Pong!")

    if message.content.lower().startswith(command_prefix + "set"):
        args_seperated = message.content.lower().split(command_prefix + "set ")
        args = args_seperated[1].split(" ")
        
        if args[0] == "pingmessage":
            pingmessage = message.content.split(command_prefix + "set")[1].split("pingmessage ")
            print(message)
            
            
            c.execute("SELECT * FROM config WHERE Setting='PingMessage' AND ServerID=?", (message.channel.server.id,))
            if c.fetchone() != None:
                if len(pingmessage) == 1 or pingmessage[1].lower() == "clear" or pingmessage[1].lower() == "reset" or pingmessage[1].lower() == "default":
                    c.execute("UPDATE config SET Value=NULL WHERE ServerID=? AND Setting='PingMessage'", (message.channel.server.id,)) 
                else:    
                    c.execute("UPDATE config SET Value=? WHERE ServerID=? AND Setting='PingMessage'", (pingmessage[1], message.channel.server.id,)) 
                    connection.commit()
            else:
                config_change = (message.channel.server.id, message.channel.server.name, "PingMessage", pingmessage[1])
                c.execute("INSERT INTO config VALUES (?,?,?,?)", config_change)
                print(config_change)
                connection.commit()
            
            



client.run(config.token)
connection.close()